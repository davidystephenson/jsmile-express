/* global describe, it, beforeEach */

const assert = require('assert')
const express = require('express')
const jsmileExpress = require('../src/index')
const jsdom = require('jsdom')
const path = require('path')

// TODO Express cache
// TODO Add standard module as dependency
// TODO Adopt method? adopt = (view, child) => include(view, { child })
// TODO Render method render = view, options => build(include(view, options))

;(() => {
  const checker = (string, done) => (error, html) => {
    if (error) throw error

    assert.strictEqual(string, html)

    done()
  }

  describe('JSMiLe-express', () => {
    const app = express()
    const viewDirectory = './test/views'
    app.set('views', viewDirectory)
    app.set('view engine', 'js')
    const engine = jsmileExpress(app, 'test/public/views.js')
    app.engine('js', engine)

    const renderer = (view, string, options) => done => options
      ? app.render(view, options, checker(string, done))
      : app.render(view, checker(string, done))

    it('should render view files by name', renderer('content', '<div>Hello world!</div>'))

    describe('include', () => {
      it(
        'should insert another view by name',
        renderer('html', '<html><head></head><body><div>Hello world!</div></body></html>')
      )

      it(
        'should support nested views',
        renderer(
          'index',
          '<html><body><div class="navbar"><a href="/">Home</a></div><h1 class="title">Welcome</h1><p>Want to buy something?</p></body></html>',
          { message: 'Want to buy something?' }
        )
      )
    })

    describe('dependency', () => {
      it(
        'should not insert content when the dependency is unregistered',
        renderer('helloWorld', '<div>hello</div>', { depend: false })
      )

      it(
        'should insert content when the dependency is registered',
        renderer(
          'helloWorld',
          '<div>hello</div><div>world</div>',
          { depend: true }
        )
      )

      it(
        'should pass options to dependencies',
        renderer('byeWorld', '<div>Goodbye my friend</div>')
      )

      it(
        'should insert a dependency multiple times if requested, passing distinct options to each',
        renderer(
          'byeOverload',
          '<div>Goodbye </div><div>Goodbye Marcus</div><div>Goodbye Andre</div>'
        )
      )
    })

    describe('depended', () => {
      it(
        'should export the list of dependencies',
        renderer('depended', 'dependency1dependency2')
      )
    })

    describe('depender', () => {
      it(
        'should return an HTML string of the integrating script tag',
        renderer(
          'depender',
          `<script type="text/javascript">'use strict';(function(){var a=JSON.parse('["firstComponent"]'),b=window.jsmile;b?b.depended&&Array.isArray(b.depended)?b.depended=b.depended.concat(a):b.depended=a:console.warn('Could not export jsmile dependencies, no jsmile object found.')})();</script>`
        )
      )

      it(
        'should return an HTML string of any integrating script tag',
        renderer(
          'secondDepender',
          `<script type="text/javascript">'use strict';(function(){var a=JSON.parse('["secondComponent"]'),b=window.jsmile;b?b.depended&&Array.isArray(b.depended)?b.depended=b.depended.concat(a):b.depended=a:console.warn('Could not export jsmile dependencies, no jsmile object found.')})();</script>`
        )
      )

      describe('integrated with jsmile-bundler', () => {
        let window

        beforeEach(() => {
          return new Promise((resolve) => {
            app.render('bundler', (error, html) => {
              if (error) throw error

              const virtualConsole = new jsdom.VirtualConsole()
              virtualConsole.sendTo(console)

              const url = path.join('file:', __dirname)

              window = new jsdom.JSDOM(
                html,
                {
                  resources: 'usable',
                  runScripts: 'dangerously',
                  url,
                  virtualConsole
                }
              ).window

              resolve()
            })
          })
        })

        it('should not trigger includers', done => {
          window.addEventListener('load', () => {
            // Test that  the dependency was inserted by the depennder
            assert.notEqual(window.jsmile.depended.indexOf('during'), -1)

            // Test that the includer did not run
            const durings = window.document.querySelectorAll('.during')
            assert.strictEqual(durings.length, 0)

            // Test that includers are running
            window.jsmile.depend('after')
            const afters = window.document.querySelectorAll('.after')
            assert.strictEqual(afters.length, 1)

            done()
          })
        })

        it('should prevent duplicating dependencies on the client-side', done => {
          window.addEventListener('load', () => {
            assert.notEqual(window.jsmile.depended.indexOf('during'), -1)

            window.jsmile.depend('during')

            const durings = window.document.querySelectorAll('.during')
            assert.strictEqual(durings.length, 0)

            done()
          })
        })

        it('should not overwrite existing client-side dependencies', done => {
          window.addEventListener('load', () => {
            assert.deepEqual(window.jsmile.depended, ['before', 'during'])

            done()
          })
        })
      })
    })
  })
})()
