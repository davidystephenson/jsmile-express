module.exports = (jsmile, window) => {
  const body = window.document.querySelector('body')

  return {
    'assets/button' () {
      jsmile.render(
        body,
        { class: 'something', child: "It doesn't matter, it won't get rendered" }
      )
    },

    somethingElse () {
      jsmile.render(body, { class: 'something', child: 'else' })
    },

    nothing () {
      jsmile.render(body, { class: 'something nothing', child: 'nothing' })
    },

    before () {
      jsmile.render(body, { class: 'before' })
    },

    during () {
      jsmile.render(body, { class: 'during' })
    },

    after () {
      jsmile.render(body, { class: 'after' })
    }
  }
}
