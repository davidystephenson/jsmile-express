(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.jsmile = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const isFunction = value => value && {}.toString.call(value) === '[object Function]'

const views = { "assets/fake": require('/home/david/Sync/jsmile-express/test/views/assets/fake.js'), "bundler": require('/home/david/Sync/jsmile-express/test/views/bundler.js'), "byeOverload": require('/home/david/Sync/jsmile-express/test/views/byeOverload.js'), "byeWorld": require('/home/david/Sync/jsmile-express/test/views/byeWorld.js'), "content": require('/home/david/Sync/jsmile-express/test/views/content.js'), "content/title": require('/home/david/Sync/jsmile-express/test/views/content/title.js'), "depended": require('/home/david/Sync/jsmile-express/test/views/depended.js'), "depender": require('/home/david/Sync/jsmile-express/test/views/depender.js'), "farewell": require('/home/david/Sync/jsmile-express/test/views/farewell.js'), "helloWorld": require('/home/david/Sync/jsmile-express/test/views/helloWorld.js'), "html": require('/home/david/Sync/jsmile-express/test/views/html.js'), "index": require('/home/david/Sync/jsmile-express/test/views/index.js'), "layout": require('/home/david/Sync/jsmile-express/test/views/layout.js'), "navbar": require('/home/david/Sync/jsmile-express/test/views/navbar.js'), "secondDepender": require('/home/david/Sync/jsmile-express/test/views/secondDepender.js') }

const includersFactory = require('/home/david/Sync/jsmile-express/test/includers.js')

const library = {
  build: require('jsml-davidystephenson'),
  browser: require('jsmile-browser'),
  views,
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  library.render = library.browser(window)
  window.jsml = library.render

  const includers = includersFactory(library, window)
  library.includers = includers

  const make = (view, options) => view
    ? view(library, options)
    : null

  const include = (name, options = {}) => {
    const view = views[name]
    const made = make(view, options)

    const includer = includers[name]
    if (includer) {
      return includer(made)
    } else {
      return made
    }
  }

  library.include = include

  library.depended = []
  library.depend = (name, options = {}) => {
    if (library.depended.indexOf(name) === -1) {
      include(name, options)

      library.depended.push(name)
    }

    return null
  }
}

module.exports = library
},{"/home/david/Sync/jsmile-express/test/includers.js":67,"/home/david/Sync/jsmile-express/test/views/assets/fake.js":68,"/home/david/Sync/jsmile-express/test/views/bundler.js":69,"/home/david/Sync/jsmile-express/test/views/byeOverload.js":70,"/home/david/Sync/jsmile-express/test/views/byeWorld.js":71,"/home/david/Sync/jsmile-express/test/views/content.js":72,"/home/david/Sync/jsmile-express/test/views/content/title.js":73,"/home/david/Sync/jsmile-express/test/views/depended.js":74,"/home/david/Sync/jsmile-express/test/views/depender.js":75,"/home/david/Sync/jsmile-express/test/views/farewell.js":76,"/home/david/Sync/jsmile-express/test/views/helloWorld.js":77,"/home/david/Sync/jsmile-express/test/views/html.js":78,"/home/david/Sync/jsmile-express/test/views/index.js":79,"/home/david/Sync/jsmile-express/test/views/layout.js":80,"/home/david/Sync/jsmile-express/test/views/navbar.js":81,"/home/david/Sync/jsmile-express/test/views/secondDepender.js":82,"jsmile-browser":65,"jsml-davidystephenson":66}],2:[function(require,module,exports){
module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};

},{}],3:[function(require,module,exports){
// 22.1.3.31 Array.prototype[@@unscopables]
var UNSCOPABLES = require('./_wks')('unscopables');
var ArrayProto = Array.prototype;
if (ArrayProto[UNSCOPABLES] == undefined) require('./_hide')(ArrayProto, UNSCOPABLES, {});
module.exports = function (key) {
  ArrayProto[UNSCOPABLES][key] = true;
};

},{"./_hide":21,"./_wks":53}],4:[function(require,module,exports){
var isObject = require('./_is-object');
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};

},{"./_is-object":26}],5:[function(require,module,exports){
// false -> Array#indexOf
// true  -> Array#includes
var toIObject = require('./_to-iobject');
var toLength = require('./_to-length');
var toAbsoluteIndex = require('./_to-absolute-index');
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};

},{"./_to-absolute-index":46,"./_to-iobject":48,"./_to-length":49}],6:[function(require,module,exports){
// 0 -> Array#forEach
// 1 -> Array#map
// 2 -> Array#filter
// 3 -> Array#some
// 4 -> Array#every
// 5 -> Array#find
// 6 -> Array#findIndex
var ctx = require('./_ctx');
var IObject = require('./_iobject');
var toObject = require('./_to-object');
var toLength = require('./_to-length');
var asc = require('./_array-species-create');
module.exports = function (TYPE, $create) {
  var IS_MAP = TYPE == 1;
  var IS_FILTER = TYPE == 2;
  var IS_SOME = TYPE == 3;
  var IS_EVERY = TYPE == 4;
  var IS_FIND_INDEX = TYPE == 6;
  var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
  var create = $create || asc;
  return function ($this, callbackfn, that) {
    var O = toObject($this);
    var self = IObject(O);
    var f = ctx(callbackfn, that, 3);
    var length = toLength(self.length);
    var index = 0;
    var result = IS_MAP ? create($this, length) : IS_FILTER ? create($this, 0) : undefined;
    var val, res;
    for (;length > index; index++) if (NO_HOLES || index in self) {
      val = self[index];
      res = f(val, index, O);
      if (TYPE) {
        if (IS_MAP) result[index] = res;   // map
        else if (res) switch (TYPE) {
          case 3: return true;             // some
          case 5: return val;              // find
          case 6: return index;            // findIndex
          case 2: result.push(val);        // filter
        } else if (IS_EVERY) return false; // every
      }
    }
    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : result;
  };
};

},{"./_array-species-create":8,"./_ctx":11,"./_iobject":24,"./_to-length":49,"./_to-object":50}],7:[function(require,module,exports){
var isObject = require('./_is-object');
var isArray = require('./_is-array');
var SPECIES = require('./_wks')('species');

module.exports = function (original) {
  var C;
  if (isArray(original)) {
    C = original.constructor;
    // cross-realm fallback
    if (typeof C == 'function' && (C === Array || isArray(C.prototype))) C = undefined;
    if (isObject(C)) {
      C = C[SPECIES];
      if (C === null) C = undefined;
    }
  } return C === undefined ? Array : C;
};

},{"./_is-array":25,"./_is-object":26,"./_wks":53}],8:[function(require,module,exports){
// 9.4.2.3 ArraySpeciesCreate(originalArray, length)
var speciesConstructor = require('./_array-species-constructor');

module.exports = function (original, length) {
  return new (speciesConstructor(original))(length);
};

},{"./_array-species-constructor":7}],9:[function(require,module,exports){
var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};

},{}],10:[function(require,module,exports){
var core = module.exports = { version: '2.6.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

},{}],11:[function(require,module,exports){
// optional / simple context binding
var aFunction = require('./_a-function');
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};

},{"./_a-function":2}],12:[function(require,module,exports){
// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};

},{}],13:[function(require,module,exports){
// Thank's IE8 for his funny defineProperty
module.exports = !require('./_fails')(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});

},{"./_fails":17}],14:[function(require,module,exports){
var isObject = require('./_is-object');
var document = require('./_global').document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};

},{"./_global":19,"./_is-object":26}],15:[function(require,module,exports){
// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');

},{}],16:[function(require,module,exports){
var global = require('./_global');
var core = require('./_core');
var hide = require('./_hide');
var redefine = require('./_redefine');
var ctx = require('./_ctx');
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;

},{"./_core":10,"./_ctx":11,"./_global":19,"./_hide":21,"./_redefine":41}],17:[function(require,module,exports){
module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};

},{}],18:[function(require,module,exports){
'use strict';
// 21.2.5.3 get RegExp.prototype.flags
var anObject = require('./_an-object');
module.exports = function () {
  var that = anObject(this);
  var result = '';
  if (that.global) result += 'g';
  if (that.ignoreCase) result += 'i';
  if (that.multiline) result += 'm';
  if (that.unicode) result += 'u';
  if (that.sticky) result += 'y';
  return result;
};

},{"./_an-object":4}],19:[function(require,module,exports){
// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

},{}],20:[function(require,module,exports){
var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};

},{}],21:[function(require,module,exports){
var dP = require('./_object-dp');
var createDesc = require('./_property-desc');
module.exports = require('./_descriptors') ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

},{"./_descriptors":13,"./_object-dp":33,"./_property-desc":40}],22:[function(require,module,exports){
var document = require('./_global').document;
module.exports = document && document.documentElement;

},{"./_global":19}],23:[function(require,module,exports){
module.exports = !require('./_descriptors') && !require('./_fails')(function () {
  return Object.defineProperty(require('./_dom-create')('div'), 'a', { get: function () { return 7; } }).a != 7;
});

},{"./_descriptors":13,"./_dom-create":14,"./_fails":17}],24:[function(require,module,exports){
// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = require('./_cof');
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};

},{"./_cof":9}],25:[function(require,module,exports){
// 7.2.2 IsArray(argument)
var cof = require('./_cof');
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};

},{"./_cof":9}],26:[function(require,module,exports){
module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

},{}],27:[function(require,module,exports){
'use strict';
var create = require('./_object-create');
var descriptor = require('./_property-desc');
var setToStringTag = require('./_set-to-string-tag');
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
require('./_hide')(IteratorPrototype, require('./_wks')('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};

},{"./_hide":21,"./_object-create":32,"./_property-desc":40,"./_set-to-string-tag":42,"./_wks":53}],28:[function(require,module,exports){
'use strict';
var LIBRARY = require('./_library');
var $export = require('./_export');
var redefine = require('./_redefine');
var hide = require('./_hide');
var Iterators = require('./_iterators');
var $iterCreate = require('./_iter-create');
var setToStringTag = require('./_set-to-string-tag');
var getPrototypeOf = require('./_object-gpo');
var ITERATOR = require('./_wks')('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};

},{"./_export":16,"./_hide":21,"./_iter-create":27,"./_iterators":30,"./_library":31,"./_object-gpo":35,"./_redefine":41,"./_set-to-string-tag":42,"./_wks":53}],29:[function(require,module,exports){
module.exports = function (done, value) {
  return { value: value, done: !!done };
};

},{}],30:[function(require,module,exports){
module.exports = {};

},{}],31:[function(require,module,exports){
module.exports = false;

},{}],32:[function(require,module,exports){
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = require('./_an-object');
var dPs = require('./_object-dps');
var enumBugKeys = require('./_enum-bug-keys');
var IE_PROTO = require('./_shared-key')('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = require('./_dom-create')('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  require('./_html').appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};

},{"./_an-object":4,"./_dom-create":14,"./_enum-bug-keys":15,"./_html":22,"./_object-dps":34,"./_shared-key":43}],33:[function(require,module,exports){
var anObject = require('./_an-object');
var IE8_DOM_DEFINE = require('./_ie8-dom-define');
var toPrimitive = require('./_to-primitive');
var dP = Object.defineProperty;

exports.f = require('./_descriptors') ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

},{"./_an-object":4,"./_descriptors":13,"./_ie8-dom-define":23,"./_to-primitive":51}],34:[function(require,module,exports){
var dP = require('./_object-dp');
var anObject = require('./_an-object');
var getKeys = require('./_object-keys');

module.exports = require('./_descriptors') ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};

},{"./_an-object":4,"./_descriptors":13,"./_object-dp":33,"./_object-keys":37}],35:[function(require,module,exports){
// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = require('./_has');
var toObject = require('./_to-object');
var IE_PROTO = require('./_shared-key')('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};

},{"./_has":20,"./_shared-key":43,"./_to-object":50}],36:[function(require,module,exports){
var has = require('./_has');
var toIObject = require('./_to-iobject');
var arrayIndexOf = require('./_array-includes')(false);
var IE_PROTO = require('./_shared-key')('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};

},{"./_array-includes":5,"./_has":20,"./_shared-key":43,"./_to-iobject":48}],37:[function(require,module,exports){
// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = require('./_object-keys-internal');
var enumBugKeys = require('./_enum-bug-keys');

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};

},{"./_enum-bug-keys":15,"./_object-keys-internal":36}],38:[function(require,module,exports){
exports.f = {}.propertyIsEnumerable;

},{}],39:[function(require,module,exports){
var getKeys = require('./_object-keys');
var toIObject = require('./_to-iobject');
var isEnum = require('./_object-pie').f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) if (isEnum.call(O, key = keys[i++])) {
      result.push(isEntries ? [key, O[key]] : O[key]);
    } return result;
  };
};

},{"./_object-keys":37,"./_object-pie":38,"./_to-iobject":48}],40:[function(require,module,exports){
module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

},{}],41:[function(require,module,exports){
var global = require('./_global');
var hide = require('./_hide');
var has = require('./_has');
var SRC = require('./_uid')('src');
var TO_STRING = 'toString';
var $toString = Function[TO_STRING];
var TPL = ('' + $toString).split(TO_STRING);

require('./_core').inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});

},{"./_core":10,"./_global":19,"./_has":20,"./_hide":21,"./_uid":52}],42:[function(require,module,exports){
var def = require('./_object-dp').f;
var has = require('./_has');
var TAG = require('./_wks')('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};

},{"./_has":20,"./_object-dp":33,"./_wks":53}],43:[function(require,module,exports){
var shared = require('./_shared')('keys');
var uid = require('./_uid');
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};

},{"./_shared":44,"./_uid":52}],44:[function(require,module,exports){
var core = require('./_core');
var global = require('./_global');
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: require('./_library') ? 'pure' : 'global',
  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
});

},{"./_core":10,"./_global":19,"./_library":31}],45:[function(require,module,exports){
'use strict';
var fails = require('./_fails');

module.exports = function (method, arg) {
  return !!method && fails(function () {
    // eslint-disable-next-line no-useless-call
    arg ? method.call(null, function () { /* empty */ }, 1) : method.call(null);
  });
};

},{"./_fails":17}],46:[function(require,module,exports){
var toInteger = require('./_to-integer');
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};

},{"./_to-integer":47}],47:[function(require,module,exports){
// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};

},{}],48:[function(require,module,exports){
// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = require('./_iobject');
var defined = require('./_defined');
module.exports = function (it) {
  return IObject(defined(it));
};

},{"./_defined":12,"./_iobject":24}],49:[function(require,module,exports){
// 7.1.15 ToLength
var toInteger = require('./_to-integer');
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};

},{"./_to-integer":47}],50:[function(require,module,exports){
// 7.1.13 ToObject(argument)
var defined = require('./_defined');
module.exports = function (it) {
  return Object(defined(it));
};

},{"./_defined":12}],51:[function(require,module,exports){
// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = require('./_is-object');
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};

},{"./_is-object":26}],52:[function(require,module,exports){
var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};

},{}],53:[function(require,module,exports){
var store = require('./_shared')('wks');
var uid = require('./_uid');
var Symbol = require('./_global').Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;

},{"./_global":19,"./_shared":44,"./_uid":52}],54:[function(require,module,exports){
'use strict';
var $export = require('./_export');
var $filter = require('./_array-methods')(2);

$export($export.P + $export.F * !require('./_strict-method')([].filter, true), 'Array', {
  // 22.1.3.7 / 15.4.4.20 Array.prototype.filter(callbackfn [, thisArg])
  filter: function filter(callbackfn /* , thisArg */) {
    return $filter(this, callbackfn, arguments[1]);
  }
});

},{"./_array-methods":6,"./_export":16,"./_strict-method":45}],55:[function(require,module,exports){
'use strict';
var $export = require('./_export');
var $indexOf = require('./_array-includes')(false);
var $native = [].indexOf;
var NEGATIVE_ZERO = !!$native && 1 / [1].indexOf(1, -0) < 0;

$export($export.P + $export.F * (NEGATIVE_ZERO || !require('./_strict-method')($native)), 'Array', {
  // 22.1.3.11 / 15.4.4.14 Array.prototype.indexOf(searchElement [, fromIndex])
  indexOf: function indexOf(searchElement /* , fromIndex = 0 */) {
    return NEGATIVE_ZERO
      // convert -0 to +0
      ? $native.apply(this, arguments) || 0
      : $indexOf(this, searchElement, arguments[1]);
  }
});

},{"./_array-includes":5,"./_export":16,"./_strict-method":45}],56:[function(require,module,exports){
// 22.1.2.2 / 15.4.3.2 Array.isArray(arg)
var $export = require('./_export');

$export($export.S, 'Array', { isArray: require('./_is-array') });

},{"./_export":16,"./_is-array":25}],57:[function(require,module,exports){
'use strict';
var addToUnscopables = require('./_add-to-unscopables');
var step = require('./_iter-step');
var Iterators = require('./_iterators');
var toIObject = require('./_to-iobject');

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = require('./_iter-define')(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');

},{"./_add-to-unscopables":3,"./_iter-define":28,"./_iter-step":29,"./_iterators":30,"./_to-iobject":48}],58:[function(require,module,exports){
'use strict';
var $export = require('./_export');
var $map = require('./_array-methods')(1);

$export($export.P + $export.F * !require('./_strict-method')([].map, true), 'Array', {
  // 22.1.3.15 / 15.4.4.19 Array.prototype.map(callbackfn [, thisArg])
  map: function map(callbackfn /* , thisArg */) {
    return $map(this, callbackfn, arguments[1]);
  }
});

},{"./_array-methods":6,"./_export":16,"./_strict-method":45}],59:[function(require,module,exports){
var DateProto = Date.prototype;
var INVALID_DATE = 'Invalid Date';
var TO_STRING = 'toString';
var $toString = DateProto[TO_STRING];
var getTime = DateProto.getTime;
if (new Date(NaN) + '' != INVALID_DATE) {
  require('./_redefine')(DateProto, TO_STRING, function toString() {
    var value = getTime.call(this);
    // eslint-disable-next-line no-self-compare
    return value === value ? $toString.call(this) : INVALID_DATE;
  });
}

},{"./_redefine":41}],60:[function(require,module,exports){
// 21.2.5.3 get RegExp.prototype.flags()
if (require('./_descriptors') && /./g.flags != 'g') require('./_object-dp').f(RegExp.prototype, 'flags', {
  configurable: true,
  get: require('./_flags')
});

},{"./_descriptors":13,"./_flags":18,"./_object-dp":33}],61:[function(require,module,exports){
'use strict';
require('./es6.regexp.flags');
var anObject = require('./_an-object');
var $flags = require('./_flags');
var DESCRIPTORS = require('./_descriptors');
var TO_STRING = 'toString';
var $toString = /./[TO_STRING];

var define = function (fn) {
  require('./_redefine')(RegExp.prototype, TO_STRING, fn, true);
};

// 21.2.5.14 RegExp.prototype.toString()
if (require('./_fails')(function () { return $toString.call({ source: 'a', flags: 'b' }) != '/a/b'; })) {
  define(function toString() {
    var R = anObject(this);
    return '/'.concat(R.source, '/',
      'flags' in R ? R.flags : !DESCRIPTORS && R instanceof RegExp ? $flags.call(R) : undefined);
  });
// FF44- RegExp#toString has a wrong name
} else if ($toString.name != TO_STRING) {
  define(function toString() {
    return $toString.call(this);
  });
}

},{"./_an-object":4,"./_descriptors":13,"./_fails":17,"./_flags":18,"./_redefine":41,"./es6.regexp.flags":60}],62:[function(require,module,exports){
// https://github.com/tc39/proposal-object-values-entries
var $export = require('./_export');
var $entries = require('./_object-to-array')(true);

$export($export.S, 'Object', {
  entries: function entries(it) {
    return $entries(it);
  }
});

},{"./_export":16,"./_object-to-array":39}],63:[function(require,module,exports){
var $iterators = require('./es6.array.iterator');
var getKeys = require('./_object-keys');
var redefine = require('./_redefine');
var global = require('./_global');
var hide = require('./_hide');
var Iterators = require('./_iterators');
var wks = require('./_wks');
var ITERATOR = wks('iterator');
var TO_STRING_TAG = wks('toStringTag');
var ArrayValues = Iterators.Array;

var DOMIterables = {
  CSSRuleList: true, // TODO: Not spec compliant, should be false.
  CSSStyleDeclaration: false,
  CSSValueList: false,
  ClientRectList: false,
  DOMRectList: false,
  DOMStringList: false,
  DOMTokenList: true,
  DataTransferItemList: false,
  FileList: false,
  HTMLAllCollection: false,
  HTMLCollection: false,
  HTMLFormElement: false,
  HTMLSelectElement: false,
  MediaList: true, // TODO: Not spec compliant, should be false.
  MimeTypeArray: false,
  NamedNodeMap: false,
  NodeList: true,
  PaintRequestList: false,
  Plugin: false,
  PluginArray: false,
  SVGLengthList: false,
  SVGNumberList: false,
  SVGPathSegList: false,
  SVGPointList: false,
  SVGStringList: false,
  SVGTransformList: false,
  SourceBufferList: false,
  StyleSheetList: true, // TODO: Not spec compliant, should be false.
  TextTrackCueList: false,
  TextTrackList: false,
  TouchList: false
};

for (var collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
  var NAME = collections[i];
  var explicit = DOMIterables[NAME];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  var key;
  if (proto) {
    if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues);
    if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
    Iterators[NAME] = ArrayValues;
    if (explicit) for (key in $iterators) if (!proto[key]) redefine(proto, key, $iterators[key], true);
  }
}

},{"./_global":19,"./_hide":21,"./_iterators":30,"./_object-keys":37,"./_redefine":41,"./_wks":53,"./es6.array.iterator":57}],64:[function(require,module,exports){
"use strict";

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es7.object.entries");

require("core-js/modules/es6.array.map");

require("core-js/modules/es6.array.filter");

require("core-js/modules/es6.regexp.to-string");

require("core-js/modules/es6.date.to-string");

require("core-js/modules/es6.array.is-array");

require("core-js/modules/es6.array.index-of");

var SELF_CLOSING_TAGS = ['area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'];

var isSelfClosing = function isSelfClosing(tag) {
  return SELF_CLOSING_TAGS.indexOf(tag) > -1;
};

var is = function is(value) {
  return value;
};

var isString = function isString(value) {
  return typeof value === 'string';
};

var isNumber = function isNumber(value) {
  return !isNaN(value) && typeof value === 'number';
};

var isAnyObject = function isAnyObject(value) {
  return value === Object(value);
};

var isValid = function isValid(value) {
  return isString(value) || isNumber(value) || isAnyObject(value);
};

var isArray = function isArray(value) {
  return Array.isArray(value);
};

var isFunction = function isFunction(value) {
  return value && {}.toString.call(value) === '[object Function]';
};

var isObject = function isObject(value) {
  return !Array.isArray(value) && !isFunction(value) && isAnyObject(value);
};

var buildProperties = function buildProperties(properties) {
  return properties.map(function (entry) {
    return buildProperty(entry);
  }).filter(is).join(' ');
};

var buildTag = function buildTag(element) {
  element.tag = "<".concat(element.data.tag); // Attach the the data properties as HTML attributes.

  if (element.data.className) {
    element.data.class = element.data.className;
  }

  var filter = function filter(property) {
    return function (entry) {
      return entry[0] !== property;
    };
  };

  element.properties = Object.entries(element.data).filter(filter('tag')).filter(filter('className'));
  element.properties = element.selfClosing ? element.properties : element.properties.filter(filter('child'));

  if (element.properties.length) {
    var properties = buildProperties(element.properties);

    if (properties.length) {
      element.tag += " ".concat(properties);
    }
  }

  element.tag += '>';
  return element.tag;
};

var formatProperty = function formatProperty(name, property) {
  return "".concat(name, "=\"").concat(property, "\"");
};

var parse = function parse(value) {
  var delimiter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

  if (isNumber(value)) {
    return value.toString();
  } else if (isFunction(value)) {
    return build(value(), {
      delimiter: delimiter
    });
  } else if (isString(value)) {
    return value;
  } else if (isArray(value)) {
    var result = value.filter(isValid).map(build).filter(isValid).join(delimiter);
    return result;
  } else {
    return null;
  }
};

var buildProperty = function buildProperty(entry) {
  var name = entry[0];
  var value = entry[1];

  if (value === '' || value === true) {
    return name;
  } else {
    var parsed = parse(value, ' ');

    if (parsed) {
      return formatProperty(name, parsed.toString());
    } else if (isObject(value)) {
      var entries = Object.entries(value).map(function (entry) {
        return ["".concat(name, "-").concat(entry[0]), entry[1]];
      });
      return buildProperties(entries);
    } else {
      return null;
    }
  }
};

var build = function build(data, options) {
  var delimiter = options && options.delimiter ? options.delimiter : '';
  var result = parse(data, delimiter);

  if (result) {
    return result;
  } else if (isObject(data)) {
    if (!data.tag) {
      data.tag = 'div';
    }

    var element = {
      data: data
    };
    element.selfClosing = isSelfClosing(data.tag);
    var output = buildTag(element);

    if (!element.selfClosing) {
      if (data.child) {
        output += build(data.child);
      }

      output += "</".concat(data.tag, ">");
    }

    return output;
  }
};

module.exports = build;

},{"core-js/modules/es6.array.filter":54,"core-js/modules/es6.array.index-of":55,"core-js/modules/es6.array.is-array":56,"core-js/modules/es6.array.iterator":57,"core-js/modules/es6.array.map":58,"core-js/modules/es6.date.to-string":59,"core-js/modules/es6.regexp.to-string":61,"core-js/modules/es7.object.entries":62,"core-js/modules/web.dom.iterable":63}],65:[function(require,module,exports){
const jsml = require('jsml-davidystephenson')

const is = value => value
const isString = value => typeof value === 'string'
const isNumber = value => !isNaN(value) && typeof value === 'number'
const isAnyObject = value => value === Object(value)
// const isValid = value => isString(value) || isNumber(value) || isAnyObject(value)

const isArray = value => Array.isArray(value)
const isFunction = value => value && {}.toString.call(value) === '[object Function]'
const isObject = value => !Array.isArray(value) &&
  !isFunction(value) &&
  isAnyObject(value)

// const isElement = value => typeof Element === 'object'
//   ? value instanceof Element
//   : value &&
//     typeof value === 'object' &&
//     value !== null &&
//     value.nodeType === 1 &&
//     typeof value.nodeName === 'string'

const strip = (data, keys) => {
  const clone = Object.assign({}, data)
  const values = {}

  if (clone) {
    keys.map(key => {
      const value = clone[key]

      if (value) {
        values[key] = value
        clone[key] = null
      }
    })
  }

  return [clone, values]
}

const enrich = (element, data, enricher) => {
  if (data) {
    Object.entries(data).map(entry => {
      const key = entry[0]
      const value = entry[1]

      enricher(element, key, value)
    })
  }
}

const listen = (element, key, value) => element.addEventListener(key, value)

const renderer = window => {
  const isNode = value => typeof window.Node === 'object'
    ? value instanceof window.Node
    : value &&
      typeof value === 'object' &&
      typeof value.nodeType === 'number' &&
      typeof value.nodeName === 'string'
  const isJquery = value => window.jQuery && value instanceof window.jQuery

  const build = data => {
    const [stripped, values] = strip(data, ['child', 'on'])

    const html = jsml(stripped)

    const template = window.document.createElement('template')
    template.innerHTML = html

    const element = template.content.firstChild

    enrich(element, values.on, listen)
    append(element, values.child)

    return element
  }

  const append = (a, b) => {
    if (a && b) {
      if (isJquery(a) || isNode(a)) {
        if (isJquery(b)) {
          b.each((index, element) => a.append(element))
        } else if (isNode(b)) {
          a.append(b)
        } else {
          const c = make(b)

          append(a, c)
        }
      } else {
        const d = make(a)

        append(d, b)
      }
    }
  }

  const make = data => {
    if (isNode(data) || isJquery(data)) {
      return data
    } else if (isArray(data)) {
      const fragment = window.document.createDocumentFragment()

      data
        .map(make)
        .map(node => append(fragment, node))

      return fragment
    } else if (isFunction(data)) {
      return make(data())
    } else if (isObject(data)) {
      return build(data)
    } else if (isString(data)) {
      return window.document.createTextNode(data)
    } else if (isNumber(data)) {
      return make(data.toString())
    } else {
      return null
    }
  }

  return (data, ...children) => {
    const base = make(data)

    children
      .map(make)
      .filter(is)
      .map(child => append(base, child))

    return base
  }
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  window.jsml = renderer(window)
}

module.exports = renderer

},{"jsml-davidystephenson":64}],66:[function(require,module,exports){
arguments[4][64][0].apply(exports,arguments)
},{"core-js/modules/es6.array.filter":54,"core-js/modules/es6.array.index-of":55,"core-js/modules/es6.array.is-array":56,"core-js/modules/es6.array.iterator":57,"core-js/modules/es6.array.map":58,"core-js/modules/es6.date.to-string":59,"core-js/modules/es6.regexp.to-string":61,"core-js/modules/es7.object.entries":62,"core-js/modules/web.dom.iterable":63,"dup":64}],67:[function(require,module,exports){
module.exports = (jsmile, window) => {
  const body = window.document.querySelector('body')

  return {
    'assets/button' () {
      jsmile.render(
        body,
        { class: 'something', child: "It doesn't matter, it won't get rendered" }
      )
    },

    somethingElse () {
      jsmile.render(body, { class: 'something', child: 'else' })
    },

    nothing () {
      jsmile.render(body, { class: 'something nothing', child: 'nothing' })
    },

    before () {
      jsmile.render(body, { class: 'before' })
    },

    during () {
      jsmile.render(body, { class: 'during' })
    },

    after () {
      jsmile.render(body, { class: 'after' })
    }
  }
}

},{}],68:[function(require,module,exports){
module.exports = () => ({ child: 'world' })

},{}],69:[function(require,module,exports){
module.exports = jsmile => ({
  tag: 'html',
  child: [
    {
      tag: 'head',
      child: [
        { tag: 'script', src: './test/jsmile.js' },
        { tag: 'script', child: `jsmile.depend('before')` },
        jsmile.depender,
        jsmile.depend('during')
      ]
    },
    { tag: 'body' }
  ]
})

},{}],70:[function(require,module,exports){
module.exports = (jsmile, options) => [
  jsmile.depend('farewell'),
  jsmile.dependency('farewell', { addressee: '' }),
  jsmile.dependency('farewell', { addressee: 'Marcus' }),
  jsmile.dependency('farewell', { addressee: 'Andre' })
]

},{}],71:[function(require,module,exports){
module.exports = (jsmile, options) => [
  jsmile.depend('farewell'),
  jsmile.dependency('farewell', { addressee: 'my friend' })
]

},{}],72:[function(require,module,exports){
module.exports = () => ({
  child: 'Hello world!'
})

},{}],73:[function(require,module,exports){
module.exports = (jsmile, options) => ({
  class: 'title',
  tag: 'h1',
  child: options.text
})

},{}],74:[function(require,module,exports){
module.exports = jsmile => [
  jsmile.depended,
  jsmile.depend('dependency1'),
  jsmile.depend('dependency2')
]

},{}],75:[function(require,module,exports){
module.exports = jsmile => [
  jsmile.depender,
  jsmile.depend('firstComponent')
]

},{}],76:[function(require,module,exports){
module.exports = (jsmile, options = {}) => ({
  child: `Goodbye ${options.addressee}`
})

},{}],77:[function(require,module,exports){
module.exports = (jsmile, options) => [
  options.depend
    ? jsmile.depend('assets/fake')
    : null,
  { child: 'hello' },
  jsmile.dependency('assets/fake')
]

},{}],78:[function(require,module,exports){
module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    { tag: 'head' },
    {
      tag: 'body',
      child: jsmile.include('content')
    }
  ]
})

},{}],79:[function(require,module,exports){
module.exports = (jsmile, options = {}) => jsmile.include('layout', {
  body: [
    jsmile.include('content/title', { text: 'Welcome' }),
    { tag: 'p', child: options.message }
  ]
})

},{}],80:[function(require,module,exports){
module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    options.head
      ? { tag: 'head', child: options.head }
      : null,
    {
      tag: 'body',
      child: [
        jsmile.include('navbar'),
        options.body
      ]
    }
  ]
})

},{}],81:[function(require,module,exports){
module.exports = () => ({
  class: 'navbar',
  child: [
    {
      tag: 'a',
      href: '/',
      child: 'Home'
    }
  ]
})

},{}],82:[function(require,module,exports){
module.exports = jsmile => [
  jsmile.depender,
  jsmile.depend('secondComponent')
]

},{}]},{},[1])(1)
});
