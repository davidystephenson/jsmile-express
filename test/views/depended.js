module.exports = jsmile => [
  jsmile.depended,
  jsmile.depend('dependency1'),
  jsmile.depend('dependency2')
]
