module.exports = () => ({
  class: 'navbar',
  child: [
    {
      tag: 'a',
      href: '/',
      child: 'Home'
    }
  ]
})
