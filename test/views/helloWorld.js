module.exports = (jsmile, options) => [
  options.depend
    ? jsmile.depend('assets/fake')
    : null,
  { child: 'hello' },
  jsmile.dependency('assets/fake')
]
