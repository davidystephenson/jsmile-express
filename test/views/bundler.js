module.exports = jsmile => ({
  tag: 'html',
  child: [
    {
      tag: 'head',
      child: [
        { tag: 'script', src: './test/jsmile.js' },
        { tag: 'script', child: `jsmile.depend('before')` },
        jsmile.depender,
        jsmile.depend('during')
      ]
    },
    { tag: 'body' }
  ]
})
