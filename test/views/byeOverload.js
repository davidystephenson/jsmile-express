module.exports = (jsmile, options) => [
  jsmile.depend('farewell'),
  jsmile.dependency('farewell', { addressee: '' }),
  jsmile.dependency('farewell', { addressee: 'Marcus' }),
  jsmile.dependency('farewell', { addressee: 'Andre' })
]
