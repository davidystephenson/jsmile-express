module.exports = (jsmile, options) => ({
  class: 'title',
  tag: 'h1',
  child: options.text
})
