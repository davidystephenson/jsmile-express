module.exports = (jsmile, options = {}) => jsmile.include('layout', {
  body: [
    jsmile.include('content/title', { text: 'Welcome' }),
    { tag: 'p', child: options.message }
  ]
})
