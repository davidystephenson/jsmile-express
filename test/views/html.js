module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    { tag: 'head' },
    {
      tag: 'body',
      child: jsmile.include('content')
    }
  ]
})
