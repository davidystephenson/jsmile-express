# JSMiLe
### J:)


A JavaScript Markup Language implementation with express integration.

## Installation
### Node
`npm install jsmile-express`

## Usage
### Initialization
`jsml-express` integrates with `express` as a view engine.
After setting a views directory, declare a view engine that will read `.js` files in the directory.
Then, initialize the engine by passing the express application itself to jsmile's `express` method.

```
const express = require('express')
const jsmile = require('jsmile-express')

const app = express()

app.set('views', './views')
app.set('view engine', 'js')
app.engine('js', jsmile(app))
```

### Rendering
You can then use the `express.render` method to render view files by name, optionally passing any data the view uses.
View files are JavaScript modules that export a function.

The function is called when the view is rendered by your routing logic.
The output is parsed by the `jsmile.build` method and then sent as the request's response.
The function is passed two arguments:
1. An object containing view extending functionality, conventionally called `jsmile`.
1. An object containing the data passed to the route, conventionally called `options`.

`views/index.js`
```
module.exports = (jsmile, options) => {
  const title = options.message.toUpperCase()

  return {
    tag: 'html',
    child: [
      { tag: 'head' },
      { 
        tag: 'body',
        child: {
          tag: 'h1',
          child: title 
        }
      }
    ]
  }
}
```

```
app.get('/', (request, response) => response.render('index', { message: 'Hello user!' }))
// <html><head></head><body><h1>HELLO USER!</h2></body></html>
```

### Building
The `jsmile` object passed to views provides a `build` method.
The method is an alias for the [`jsml-davidystephenson`]() package.
```
jsmile.build('hello world')
// 'hello world'

jsmile.build({})
// '<div></div>'

const jsmile.build({ child: 'hello world' })
// '<div>hello world</div>'

jsmile.build({ tag: 'h1' child: 'Welcome!' })
// <h1>Welcome!</h1>

jsmile.build({ tag: 'img', src: 'imgur.com' })
// <img src='imgur.com'>

const format = name => [
  { tag: 'h3', class: 'title', child: 'User page' }, 
  { tag: 'p', child: `Welcome ${name.toUppercase()}.`}
]
jsmile.build(format('someone'))
// '<h3 class="title">User page</h3> <p>Welcome SOMEONE.</p>'
```

### Including
The `jsmile` object passed to views provides a method `include`.
`include` allows other views to be incorporated.
It works similarly to the Express `render` method.
It takes two arguments:
1. The name of the view.
2. An optional object with data used in the function.

`views/navbar.js`
```
module.exports = () => ({
  class: 'navbar',
  child: [
    {
      tag: 'a',
      href: '/',
      child: 'Home'
    }
  ]
})
```

`views/layout.js`
```
module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    { tag: 'head', child: options.head },
    {
      tag: 'body',
      child: [
        jsmile.include('navbar'),
        options.body
      ]
    }
  ]
})
```

`views/title.js`
```
module.exports = (jsmile, options) => ({
  class: 'title',
  tag: 'h1',
  child: options.text
})
```

`views/index.js`
```
module.exports = (jsmile, options) => jsmile.include('layout', {
  body: [
    jsmile.include('title', { text: 'Welcome'}),
    { tag: 'p', child: options.message }
  ]
})
```

```
app.get('/', (request, response) => response.render('index', { message: 'Hello World!' }))
///<html><head></head><body><div class="navbar"><a href="/">Home</a></div><h1 class="title">Welcome</h1><p>Hello World!</p></body></html>
```

### Depending
The `jsmile` library passed to views also provides the methods `depend` and `dendepency`.
These methods allow you to conditionally include views across views.

`depend` takes 1 arguments, the name of a view.
Passing it the name of a view to registeres that the view should be included elsewhere during this rendering.
It returns `null`, and can be called inside a view's returned content with no effect.

`dependency` takes the same two arguments as `app.render`:
1. The name of a view.
2. An optional options object to pass to the view.

If a view is registered by `depend` during a rendering, all `dependency` calls for that view will execute its function, passing the options if defined.
The output of the view function will be returned and included where `dependency` was called.
If the view is not registered by `depend`, `dependency` will return `null`.

`views/justButtonPage.js`
```
module.exports = (jsmile, options) => {
  tag: 'html',
  child: [
    {
      tag: 'head',
      child: jsmile.dependency('assets/button', { behavior: true })
    },
    {
      tag: 'body',
      child: jsmile.include('button', { value: 'I warned you' })
    }
  ]
}
```

`views/button.js`
```
module.exports = (jsmile, options) => [
  {
    tag: 'button',
    type: 'button',
    value: options.value
  },
  jsmile.depend('assets/button')
]
```

`views/assets/button.js`
```
module.exports = (jsmile, options) => {
  const assets = [
    {
      tag: 'link',
      rel: 'stylesheet',
      type: 'text/css',
      href: '/button.css'
    },
  ]
  
  if (options.behavior) {
    assets.push({ tag: 'script', src: '/button.js' })
  }

  return assets
}
```

```
app.render('justButtonPage')
// '<html><head><link rel="stylesheet" type="text/css" href="/button.css"><script src="/button.js"></script></head><body><button type="button" value="I warned you"></button></body></html>'
```

The list of views that were depended is available at `jsmile.depended`.
It may be useful to pass the list of depended view to the client side in some way to avoid duplicate imports.

TRhe list of depended views can be inserted into the `jsmile`'s global client side object using the `jsmile.depender`property. 
`jsmile.depender` is a script element defined as a `jsmile` object.
The script attaches the list of depended views to the browser's `window.jsmile.depended` property.

`views/assets.js`
```
module.exports = jsmile => [
  { tag: 'script', src: './custom-bundled-jsmile.js' },
  jsmile.depender
]
```
 
`views/component1.js`
```
module.exports = jsmile => ({
  class: 'component-1',
  child: jsmile.depend('component1')
})
```

`views/content.js`
```
module.exports = jsmile => [
  jsmile.include('assets'),
  jsmile.include('component1')
]
```

```
app.render('content')
// <script src='./custom-bundled-jsmile.js'></script><script type="text/javascript">'use strict';(function(){var a=JSON.parse('["component1"]'),b=window.jsmile;b?(f,b.depended&&Array.isArray(b.depended)?b.depended.concat(a):b.depended=a):console.warn('Could not export jsmile dependencies, no jsmile object found.')})();</script><div class="component-1"></div>
```

This smoothly integrates with [`jsmile-bundler`](), allowing you to avoid duplicating depended content before and after server side rendering.

`views/assets.js`
```
module.exports = jsmile => [
  { tag: 'script', src: './custom-bundled-jsmile.js' },
  jsmile.depender()
]
```
 
`views/component1.js`
```
module.exports = jsmile => ({
  class: 'component-1',
  child: jsmile.depend('component1')
})
```

`views/content.js`
```
module.exports = jsmile => [
  jsmile.include('assets'),
  jsmile.include('component1')
]
```

```
app.render('content')
// <script src='./custom-bundled-jsmile.js'></script><script type="text/javascript">'use strict';(function(){var a=JSON.parse('["component1"]'),b=window.jsmile;b?(f,b.depended&&Array.isArray(b.depended)?b.depended.concat(a):b.depended=a):console.warn('Could not export jsmile dependencies, no jsmile object found.')})();</script><div class="component-1"></div>
```

`views/justButtonPage.js`
```
module.exports = (jsmile, options) => {
  tag: 'html',
  child: [
    {
      tag: 'head',
      child: [
        { tag: 'script', src: './custom-bundled-jsmile.js' },
        jsmile.depender(),
        jsmile.dependency('assets/button', { behavior: true })
      ]
    },
    {
      tag: 'body',
      child: jsmile.include('button', { value: 'I warned you' })
    }
  ]
}
```

`views/button.js`
```
module.exports = (jsmile, options) => [
  {
    tag: 'button',
    type: 'button',
    value: options.value
  },
  jsmile.depend('assets/button')
]
```

`views/assets/button.js`
```
module.exports = (jsmile, options) => {
  const assets = [
    {
      tag: 'link',
      rel: 'stylesheet',
      type: 'text/css',
      href: '/button.css'
    },
  ]
  
  if (options.behavior) {
    assets.push({ tag: 'script', src: '/button.js' })
  }

  return assets
}
```

`includers.js`
```
module.exports = (jsmile, window) => {
  const body = window.document.querySelector('body')

  return {
    'assets/button' () {
      jsmile.render(body, { class: 'something', child: "It doesn't matter, it won't get rendered" })
    },

    somethingElse () {
      jsmile.render(body, { class: 'something', child: 'else' })
    }
  }
}
```

```
jsmile-bundler views includers.js public/custom-bunded-jsmile.js
```

```
app.render('justButtonPage')
// '<html><head><script src='./custom-bundled-jsmile.js'></script><script type="text/javascript">'use strict';(function(){var a=JSON.parse('["component1"]'),b=window.jsmile;b?b.depended&&Array.isArray(b.depended)?b.depended.concat(a):b.depended=a:console.warn('Could not export jsmile dependencies, no jsmile object found.')})();</script><link rel="stylesheet" type="text/css" href="/button.css"><script src="/button.js"></script></head><body><button type="button" value="I warned you"></button></body></html>'
```

`public/button.js`
```
console.log(jsmile.depended)
// ['assets/button']

document.querySelector('button').on('click', () => {
  jsmile.depend('assets/button')
  jsmile.depend('somethingElse')
})
button.click()

console.log(jsmile.depended)
// ['assets/button', 'somethingElse']

const something = window.document.querySelector('.something')
console.log(something.textContent)
// 'else'
```
