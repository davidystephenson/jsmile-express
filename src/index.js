const build = require('jsml-davidystephenson')
const path = require('path')

module.exports = app => {
  const viewDirectory = app.get('views')
  const basePath = path.join(process.cwd(), viewDirectory)

  return (filepath, options, callback) => {
    const state = {}
    state.dependencies = new Set()

    const depend = name => {
      state.dependencies.add(name)

      return null
    }

    state.wet = false

    const dependency = (name, options) => () => {
      if (state.wet && state.dependencies.has(name)) {
        return read(name, options)
      } else {
        return null
      }
    }

    const include = (name, options) => read(name, options)

    const jsmile = { build, include, depend, dependency, depended: [], depender: {} }

    const read = (view, options) => require(path.join(basePath, view))(jsmile, options)

    const view = require(filepath)

    // Register dependencies
    view(jsmile, options)
    jsmile.depended = Array.from(state.dependencies)
    jsmile.depender = {
      tag: 'script',
      type: 'text/javascript',
      child: `'use strict';(function(){var a=JSON.parse('${JSON.stringify(jsmile.depended)}'),b=window.jsmile;b?b.depended&&Array.isArray(b.depended)?b.depended=b.depended.concat(a):b.depended=a:console.warn('Could not export jsmile dependencies, no jsmile object found.')})();`
    }

    state.wet = true
    const output = view(jsmile, options)

    const rendered = build(output)

    return callback(null, rendered)
  }
}
